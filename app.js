// se importan las librerias a usar
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

require("dotenv/config");
const authJwt = require('./jwt/jwt')

// se crea una constante que contiene todas las funciones de express
const app = express();
// se habilita cors para recibir peticiones de otro servidor
app.use(cors());
app.options('*', cors);

// middleware 
app.use(express.json());
app.use(authJwt());
app.use('/public/uploads', express.static(__dirname + '/public/uploads'));

// definimos la ruta al controlador
const prodRoutes = require('./routers/productos');
const userRoutes = require('./routers/usuarios');
const proveedorRoutes = require('./routers/proveedores');
const configRoutes = require('./routers/configuraciones');

// definimos las url a la que nos vamos a conectar
app.use('/producto', prodRoutes);
app.use('/usuario', userRoutes);
app.use('/proveedor', proveedorRoutes);
app.use('/configuracion', configRoutes);

// parametros para conectarse a la base de datos
const connectionParams = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  dbName: "petshop",
};

// definimos la conexion a la base de datos
mongoose.connect(process.env.CONNECT, connectionParams).then(() => {
  console.log("Conectado a mongoDB");
}).catch((err) => console.log(err));

// definimos el puerto a usar
const PORT = process.env.PORT || 5000;


// el servidor se inicia en el puerto asignado
app.listen(PORT, () => {
  console.log(`El servidor esta corriendo en http://localhost:${PORT}`);
});
