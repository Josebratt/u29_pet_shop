const mongoose = require('mongoose');

const proveedorEsquema = mongoose.Schema(
    {
        nombre:{
            type: String,
            required: true
        },
        apellido:{
            type: String,
            required: true
        },
        tipoDoc:{
            type: String,
            required: true
        },
        numDoc:{
            type: Number,
            required: true
        },
        direccion:{
            type: String,
            required: true
        },
        telefono:{
            type: Number,
            required: true
        },
        email:{
            type: String,
            required: true
        }
    }
);

module.exports = mongoose.model("Proveedores", proveedorEsquema);