const mongoose = require('mongoose');

const usuarioSchema = mongoose.Schema(
    {
        usuario: {
            type: String,
        },
        clave: {
            type: String,
            required: true
        },
        role: {
            type: String,
            enum: ['usuario', 'admin', 'transportador'],
            default: 'usuario'
        },
        nombres: {
            type: String,
            required: true
        },
        apellidos: {
            type: String,
            required: true
        },
        tipoDoc: {
            type: String,
        },
        numDoc: {
            type: String,
        },
        direccion: {
            type: String,
        },
        telefono: {
            type: String,
        },
        email: {
            type: String,
            unique: true,
            required: true
        },
        estado: {
            type: Boolean,
            default: true
        }
    }
);

//Con esto exportamos el esquema creado anteriormente
module.exports = mongoose.model("Usuarios", usuarioSchema);
