const mongoose = require('mongoose');

const configSchema = mongoose.Schema({

        valorEnvio: {
            type: Number,
        },
        topeEnvio: {
            type: Number,
        },        
        nombreTienda: {
            type: String,
            required: true
        }
    }
);

module.exports = mongoose.model('Configuraciones', configSchema);