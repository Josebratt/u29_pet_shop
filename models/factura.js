const mongoose = require('mongoose');

const facturaSchema = mongoose.Schema({

    codigo: {
        type: Number,
    },
    fecha: {
        type: Date,
        default: Date.now,
    },        
    usuario: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Usuarios'
    },
    estado: {
        type: String,
        required: true,
        default: 'Pending',
    },
    tipo: {
        type: String
    },
    valor: {
        type: Number,
    },
    detalle : [
        { productoId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Productos'
        },
        cantidad: {
            type: Number,
            default: 1
        },
    }
    ],
    fechaPreparacion: {
        type: Date
    },
    fechaEnvio: {
        type: Date
    },
    fechaEntrega: {
        type: Date
    }

}
);

module.exports = mongoose.model('Facturas', facturaSchema);