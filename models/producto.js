const mongoose = require('mongoose');

const productoSchema = mongoose.Schema({ 
    nombre: {
        type: String,
        required: true,
    },
    codigo: {
        type: String,
        required: true,
    },
    image: {
        type: String,
        default: "",
    },
    unidadMedida: {
        type: String,
        required: true,
    },
    valorCompra: {
        type: String,
        required: true,
    },
    valorVenta: {
        type: String,
        required: true,
    },
    cantidad: {
        type: String,
        required: true,
    },
    categoria: {
        type: String,
        required: true,
    },
});

module.exports = mongoose.model('Productos', productoSchema);