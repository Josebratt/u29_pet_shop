const express = require("express");
const router = express.Router();
const Producto = require("../models/producto");
const multer = require("multer");

// definimos que el archivo a subir es una imagen
const FILE_TYPE_MAP = {
  "image/png": "png",
  "image/jpeg": "jpeg",
  "image/jpg": "jpg",
};

// validamos que es una imagen y le asiganos um nombre
// asi como eliminamos cualquier espacio en blanco en el nombre
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    const isValid = FILE_TYPE_MAP[file.mimetype];
    let uploadError = new Error("invalid image type");

    if (isValid) {
      uploadError = null;
    }
    cb(uploadError, "public/uploads");
  },
  filename: function (req, file, cb) {
    const fileName = file.originalname.split(" ").join("-");
    const extension = FILE_TYPE_MAP[file.mimetype];
    //cb(null, fileName + '-' + Date.now());
    cb(null, `${fileName}-${Date.now()}.${extension}`);
  },

});

const uploadOptions = multer({ storage: storage });

// obtener todos los productos
router.get("/", (req, res) => {
  Producto.find()
    .then((data) => {
      if (data.length) {
        return res.status(200).send(data);
      } else {
        return res.status(404).json({
          success: false,
          message: "no existen productos en la base de datos",
        });
      }
    })
    .catch((err) => {
      res.status(500).json({
        success: false,
        error: err,
      });
    });
});

// obtener el detalle de un producto por su Id
router.get("/:id", (req, res) => {
  Producto.findById(req.params.id)
    .then((data) => {
      if (data) {
        return res.status(200).send(data);
      } else {
        return res.status(404).json({
          success: false,
          message: "El producto no existe en la base de datos",
        });
      }
    })
    .catch((err) => {
      res.status(500).json({
        success: false,
        error: err,
      });
    });
});

// guardar producto
router.post("/", uploadOptions.single("image"), (req, res) => {

  // validamos si existe una imagen en el request
  const file = req.file;
  if (!file) {
    return res.status(404).send("No hay imagen en el request")
  }
  // definimos el nombre del archivo y la ruta
  const fileName = req.file.filename;
  const basePath = `${req.protocol}://${req.get("host")}/public/uploads/`;

  let producto = new Producto({
    nombre: req.body.nombre,
    codigo: req.body.codigo,
    image: `${basePath}${fileName}`,
    unidadMedida: req.body.unidadMedida,
    valorCompra: req.body.valorCompra,
    valorVenta: req.body.valorVenta,
    cantidad: req.body.cantidad,
    categoria: req.body.categoria,
  });

  producto
    .save()
    .then(() => {
      res.status(201).json({
        success: true,
        message: "Producto creado exitosamente",
      });
    })
    .catch((err) => {
      res.status(500).json({
        error: err,
        success: false,
      });
    });
});

// actualizar producto
router.put("/:id", uploadOptions.single("image"), (req, res) => {

  // 
  const file = req.file;
  let imagepath;

  if (file) {
    const fileName = file.filename;
    const basePath = `${req.protocol}://${req.get('host')}/public/uploads/`;
    imagepath = `${basePath}${fileName}`;
  } else {
      imagepath = product.image;
  }

  Producto.findByIdAndUpdate(
    req.params.id,
    {
      nombre: req.body.nombre,
      codigo: req.body.codigo,
      image: imagepath,
      unidadMedida: req.body.unidadMedida,
      valorCompra: req.body.valorCompra,
      valorVenta: req.body.valorVenta,
      cantidad: req.body.cantidad,
      categoria: req.body.categoria,
    },
    { new: true }
  )
    .then(() => {
      res.status(201).json({
        success: true,
        message: "Producto actualizado exitosamente",
      });
    })
    .catch((err) => {
      res.status(500).json({
        success: false,
        error: err,
      });
    });
});

// eliminar producto
router.delete("/:id", (req, res) => {
  Producto.findByIdAndDelete(req.params.id)
    .then((data) => {
      if (data) {
        return res.status(200).json({
          success: true,
          message: "Producto eliminado exitosamente",
        });
      } else {
        return res.status(404).json({
          success: false,
          message: "Producto no encontrado",
        });
      }
    })
    .catch((err) => {
      res.status(500).json({
        success: false,
        error: err,
      });
    });
});

// contador de proveedores
router.get(`/get/count`, async (req, res) => {
  const prodCount = await Producto.countDocuments();

  console.log(prodCount);

  if (!prodCount) {
    prodCount.status(500).json({success: false})
  }
  res.send({
    proveeCount: prodCount,
  })
});

module.exports = router;
