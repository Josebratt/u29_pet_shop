const express = require("express");
const router = express.Router();
const Config = require("../models/configuracion");

//Listar Configuracion
router.get("/", (req, res) => {
  Config.find()
    .then((data) => {
      if (data.length) {
        return res.status(200).send(data);
      } else {
        return res.status(404).json({
          success: false,
          message: "no existen configuraciones en la base de datos",
        });
      }
    })
    .catch((err) => {
      res.status(500).json({
        success: false,
        error: err,
      });
    });
});

// Guardar Configuracion
router.post("/", (req, res) => {
  let configuracion = new Config({
    valorEnvio: req.body.valorEnvio,
    topeEnvio: req.body.topeEnvio,
    nombreTienda: req.body.nombreTienda
  });

  configuracion
    .save()
    .then(() => {
      res.status(201).json({
        success: true,
        message: "Configuracion creada exitosamente",
      });
    })
    .catch((err) => {
      res.status(500).json({
        error: err,
        success: false,
      });
    });
});

// Editar configuracion
router.put("/:id", (req, res) => {
  Config.findByIdAndUpdate(req.params.id, {
    valorEnvio: req.body.valorEnvio,
    topeEnvio: req.body.topeEnvio,
    nombreTienda: req.body.nombreTienda,
  })
    .then((data) => {
      if (data) {
        (success = true),
          res
            .status(200)
            .json({ message: "Configuracion actualizada correctamente" });
      } else {
        (success = false),
          res.status(400).json({ message: "Error al realizar la actualizacion" });
      }
    })
    .catch((err) => {
      res.status(500).json({ message: err });
    });
});

module.exports = router;
