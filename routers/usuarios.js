const express = require("express");
const router = express.Router();
const Usuario = require("../models/usuario");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

//Listar usuarios
router.get("/", (req, res) => {
  Usuario.find()
    .then((data) => {
      if (data) {
        res.status(200).json(data);
      } else {
        res.status(404).json({
          success: false,
          Mensaje: "No hay usuarios registrados aún",
        });
      }
    })
    .catch((err) => {
      res.status(500).json({ error: err });
    });
});

// Guardar usuario
router.post("/", (req, res) => {
  // const userExist = await Usuario.findOne(req.params.email);
  
  // if (userExist) {
  //   return res.status(409).send("el usuario ya existe");
  // }

  Usuario.create({
    usuario: req.body.usuario,
    clave: bcrypt.hashSync(req.body.clave, 10),
    role: req.body.role,
    nombres: req.body.nombres,
    apellidos: req.body.apellidos,
    tipoDoc: req.body.tipoDoc,
    numDoc: req.body.numDoc,
    direccion: req.body.direccion,
    telefono: req.body.telefono,
    email: req.body.email.toLowerCase(),
    estado: req.body.estado,
  })
    .then((data) => {
      if (data) {
        success: true,
          res.status(201).json({ message: "Registro creado exitosamente" });
      } else {
        success: false,
          res.status(400).json({ message: "Error al registrar el usuario" });
      }
    })
    .catch((err) => {
      res.status(500).json({ error: err });
    });
});

// Actualizar usuario según su id
router.put("/:id", async (req, res) => {
  const userExist = await Usuario.findById(req.params.id);
  
  let newClave;
  if (req.body.clave) {
    newClave = bcrypt.hashSync(req.body.clave, 10);
  } else {
    newClave = userExist.clave;
  }

  Usuario.findByIdAndUpdate(req.params.id, {
    usuario: req.body.usuario,
    clave: newClave,
    role: req.body.rol,
    nombres: req.body.nombres,
    apellidos: req.body.apellidos,
    tipoDoc: req.body.tipoDoc,
    numDoc: req.body.numDoc,
    direccion: req.body.direccion,
    telefono: req.body.telefono,
    email: req.body.email,
    estado: req.body.estado,
  })
    .then((data) => {
      if (data) {
        success: true,
          res
            .status(200)
            .json({ message: "El usuario se actualizó correctamente" });
      } else {
        success: false,
          res.status(400).json({ message: "Usuario no pudo ser actualizado" });
      }
    })
    .catch((err) => {
      res.status(500).json({ message: err });
    });
});

// Eliminar usuario

router.delete("/:id", (req, res) => {
  Usuario.findByIdAndDelete(req.params.id)
    .then((data) => {
      if (data) {
        return res.status(200).json({
          success: true,
          Mensaje: "El usuario fue eliminado exitosamente",
        });
      } else {
        return res.status(404).json({
          success: false,
          Mensaje: "Usuario no encontrado",
        });
      }
    })
    .catch((err) => {
      res.status(500).json({ error: err });
    });
});

// contador de usuarios
router.get(`/get/count`, async (req, res) => {
  const userCount = await Proveedor.countDocuments();

  console.log(userCount);

  if (!userCount) {
    userCount.status(500).json({success: false})
  }
  res.send({
    userCount: userCount
  })
});

// Login Ususario
router.post("/login", async (req, res) => {
  const usuario = await Usuario.findOne({ email: req.body.email });
  const secret = process.env.secret;
  console.log(usuario);

  if (!usuario) {
    return res.status(400).send("Usuario no encontrado");
  }

  if (usuario && bcrypt.compareSync(req.body.clave, usuario.clave)) {
    const token = jwt.sign({
      usuarioId: usuario.id,
      esUsuario: usuario.role
    }, secret, {
      expiresIn: "1h"
    });

    console.log("se genera el token", token);
    res.status(200).send({usuario : usuario.email, token: token});
  } else {
    res.status(400).send("Contraseña  incorrecta!");
  }

});

module.exports = router;
