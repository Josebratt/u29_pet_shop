const express = require("express");
const Proveedor = require("../models/proveedor");
const router = express.Router();

//GET - LISTAR PROVEEDORES
router.get("/", (req, res) => {
  Proveedor.find()
    .then((data) => {
      if (data.length) {
        return res.status(200).send(data);
      } else {
        return res.status(404).json({
          success: false,
          message: "no existen proveedores en la base de datos",
        });
      }
    })
    .catch((err) => {
      res.status(500).json({
        success: false,
        error: err,
      });
    });
});

// GetById - Proveedor 
router.get("/:id", (req, res) => { 
  Proveedor.findById(req.params.id).then((data) => { 
    if (data) {
      console.log(data);
      return res.status(200).send(data);
    }  else {
      return res.status(404).json({
        success: false,
        mensaje: "El proveedor no ha sido encontrado.",
      });
    }
  });
});

//POST - CREAR PROVEEDORES
router.post("/", (req, res) => {
  const proveedor = new Proveedor({
    nombre: req.body.nombre,
    apellido: req.body.apellido,
    tipoDoc: req.body.tipoDoc,
    numDoc: req.body.numDoc,
    direccion: req.body.direccion,
    telefono: req.body.telefono,
    email: req.body.email,
  });

  proveedor.save()
    .then(() => {
      res.status(201).json({
        success: true,
        message: "Proveedor creado exitosamente",
      });
    })
    .catch((err) => {
      res.status(500).json({
        error: err,
        success: false,
      });
    });
});

//DELETE - ELIMINAR PROVEEDORES
router.delete("/:id", (req, res) => {
  Proveedor.findByIdAndDelete(req.params.id)
    .then((data) => {
      if (data) {
        return res.status(200).json({
          success: true,
          mensaje: "El proveedor ha sido eliminado correctamente",
        });
      } else {
        return res.status(404).json({
          success: false,
          mensaje: "El proveedor no ha sido eliminado",
        });
      }
    })
    .catch((err) => {
      res.status(500).json({ message: err });
    });
});

//PUT - EDITAR PROVEEDORES
router.put("/:id", (req, res) => {
  Proveedor.findByIdAndUpdate(req.params.id, {
    $set: {
      nombre: req.body.nombre,
      apellido: req.body.apellido,
      tipoDoc: req.body.tipoDoc,
      numDoc: req.body.numDoc,
      direccion: req.body.direccion,
      telefono: req.body.telefono,
      email: req.body.email,
    },
  }).then(() => {
    res.status(201).json({
      success: true,
      message: "Proveedor actualizado exitosamente",
    });
  })
  .catch((err) => {
    res.status(500).json({
      success: false,
      error: err,
    });
  });
});

// contador de proveedores
router.get(`/get/count`, async (req, res) => {
  const proveeCount = await Proveedor.countDocuments();

  console.log(proveeCount);

  if (!proveeCount) {
    userCount.status(500).json({success: false})
  }
  res.send({
    proveeCount: proveeCount,
  })
});

module.exports = router;
