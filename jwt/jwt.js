const { expressjwt: expressJwt } = require("express-jwt");

function authJwt() { 
    const secret = process.env.secret;

    return expressJwt({
        secret,
        algorithms: ['HS256']
    }).unless({
        path: [
            { url: /\/public\/uploads(.*)/, methods: ['GET', 'OPTIONS'] },
            { url: /\/producto(.*)/, methods: ['GET', 'OPTIONS'] },
            { url: /\/proveedor(.*)/, methods: ['GET', 'OPTIONS'] },
            { url: /\/usuario(.*)/, methods: ['GET', 'POST', 'OPTIONS'] },
            `/login`,
            `/registro`,
        ]
    });
}

module.exports = authJwt;